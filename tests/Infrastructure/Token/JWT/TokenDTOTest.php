<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-02-25
 * Time: 12:35
 */

namespace Tests\Infrastructure\Token\JWT;


use App\Infrastructure\Token\JWT\Token;
use PHPUnit\Framework\TestCase;

class TokenDTOTest extends TestCase
{

  private $secret = 'some_secret';
  private $algorithm = 'sha256';
  private $payload = [
    'login' => 'login',
    'roles' => ['role_1', 'role_2']
  ];

  public function testCreation()
  {
    $token = new Token($this->secret, $this->algorithm, $this->payload);
    $token_dto = $token->encode();

    $this->assertArraySubset($this->payload, $token_dto->getPayload());
  }

}