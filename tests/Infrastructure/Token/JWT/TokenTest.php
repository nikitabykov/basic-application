<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-02-22
 * Time: 18:04
 */

namespace Tests\Infrastructure\Token\JWT;


use App\Infrastructure\Token\JWT\Token;
use App\Infrastructure\Token\JWT\TokenAlgorithmException;
use App\Infrastructure\Token\JWT\TokenDTO;
use App\Infrastructure\Token\JWT\TokenDTOInterface;
use App\Infrastructure\Token\JWT\TokenEmptySecretException;
use App\Infrastructure\Token\JWT\TokenFormatIncorrectException;
use App\Infrastructure\Token\JWT\TokenInterface;
use PHPUnit\Framework\TestCase;

class TokenTest extends TestCase
{
  private $secret = 'some_secret';

  private $valid_algorithms = [
    'sha256',
    'sha512',
    'md5'
  ];

  private $unvalid_algorithms = [
    'sha652',
    'sha215',
    '5dm'
  ];

  private $payload = [
    'login' => 'login',
    'roles' => ['role_1', 'role_2']
  ];

  public function testCreationWithIncorrectAlgorithm()
  {
    $algorithm = $this->getRandomUnvalidAlgorithm();
    $this->expectException(TokenAlgorithmException::class);
    new Token($this->secret, $algorithm, $this->payload);
  }

  public function testCreationWithEmptySecret()
  {
    $algorithm = $this->getRandomUnvalidAlgorithm();
    $this->expectException(TokenEmptySecretException::class);
    new Token('', $algorithm, $this->payload);
  }

  public function testCreation()
  {
    $token = new Token($this->secret);
    $this->assertInstanceOf(TokenInterface::class, $token);

    $algorithm = $this->getRandomValidAlgorithm();
    $token = new Token($this->secret, $algorithm);
    $this->assertInstanceOf(TokenInterface::class, $token);

    $algorithm = $this->getRandomValidAlgorithm();
    $token = new Token($this->secret, $algorithm, $this->payload);
    $this->assertInstanceOf(TokenInterface::class, $token);
  }

  public function testEncode()
  {
    $algorithm = $this->getRandomValidAlgorithm();
    $token = new Token($this->secret, $algorithm, $this->payload);
    $this->assertInstanceOf(TokenInterface::class, $token);

    $token_dto = $token->encode();
    $this->assertInstanceOf(TokenDTO::class, $token_dto);
  }

  public function testTokenFormat() {

    $tokens = [
      'eyJ0eXAiOiJKV1QiLCJhbGciOiJzaGEyNTYifQ==.eyJsb2dpbiI6Im5pa2l0YSIsIjAiOnsicm9sZXMiOiJhZG1pbiIsIjAiOiJlZGl0b3IifX0=.9747dddd0716760c0f02c56f9acd0f4de1db873b5f1533c6b47a3655abb3dc2a',
      'BearereyJ0eXAiOiJKV1QiLCJhbGciOiJzaGEyNTYifQ==.eyJsb2dpbiI6Im5pa2l0YSIsIjAiOnsicm9sZXMiOiJhZG1pbiIsIjAiOiJlZGl0b3IifX0=.9747dddd0716760c0f02c56f9acd0f4de1db873b5f1533c6b47a3655abb3dc2a',
      'eyJ0eXAiOiJKV1QiLCJhbGciOiJzaGEyNTYifQ==,eyJsb2dpbiI6Im5pa2l0YSIsIjAiOnsicm9sZXMiOiJhZG1pbiIsIjAiOiJlZGl0b3IifX0=,9747dddd0716760c0f02c56f9acd0f4de1db873b5f1533c6b47a3655abb3dc2a',
      'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJzaGEyNTYifQ==,eyJsb2dpbiI6Im5pa2l0YSIsIjAiOnsicm9sZXMiOiJhZG1pbiIsIjAiOiJlZGl0b3IifX0=,9747dddd0716760c0f02c56f9acd0f4de1db873b5f1533c6b47a3655abb3dc2a'
    ];

    foreach ($tokens as $token) {
      try {
        Token::decode($token, $this->secret);
      }
      catch (\Exception $e) {
        $this->assertInstanceOf(TokenFormatIncorrectException::class, $e);
      }
    }

    $token_odt = Token::decode('Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJzaGEyNTYifQ==.eyJsb2dpbiI6ImxvZ2luIiwicm9sZXMiOlsicm9sZV8xIiwicm9sZV8yIl19.9cfd1103177f4d0316ad2f9a9812ba14e5d66c3a6ff5e6113c4e1b455f6a378f', $this->secret);
    $this->assertInstanceOf(TokenDTO::class, $token_odt);
    
  }

  public function testDecode()
  {
    $algorithm = $this->getRandomValidAlgorithm();
    $token = new Token($this->secret, $algorithm, $this->payload);
    $token_dto = $token->encode();

    $this->assertInstanceOf(TokenDTOInterface::class, $token_dto);

    $token_hash = $token_dto->getToken();

    $token_dto_decoded = Token::decode($token_hash, $this->secret);
    $this->assertInstanceOf(TokenDTOInterface::class, $token_dto_decoded);
    $this->assertEquals($token_dto, $token_dto_decoded);
    $this->assertEquals($token_dto->getToken(), $token_dto_decoded->getToken());
    $this->assertArraySubset($this->payload, $token_dto_decoded->getPayload());
  }

  private function getRandomValidAlgorithm()
  {
    $algorithms = $this->valid_algorithms;
    shuffle($algorithms);
    return array_shift($algorithms);
  }

  private function getRandomUnvalidAlgorithm()
  {
    $algorithms = $this->unvalid_algorithms;
    shuffle($algorithms);
    return array_shift($algorithms);
  }
}