<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-04-02
 * Time: 11:41
 */

namespace Tests\Infrastructure\DataObject;


use App\Infrastructure\Http\Error\Error;
use PHPUnit\Framework\TestCase;

class ErrorTest extends TestCase
{

  private $error_array = [
    'code' => 100,
    'name' => 'Test Name',
    'description' => 'Test Description'
  ];

  public function testCreation()
  {
    $error = new Error($this->error_array['code'], $this->error_array['name'], $this->error_array['description']);
    $this->assertInstanceOf(Error::class, $error);
  }

  public function testCreationEquals()
  {
    $error1 = new Error($this->error_array['code'], $this->error_array['name'], $this->error_array['description']);
    $error2 = new Error($this->error_array['code'], $this->error_array['name'], $this->error_array['description']);
    $this->assertEquals($error1, $error2);
  }

  public function testConversionToArray()
  {
    $error = new Error($this->error_array['code'], $this->error_array['name'], $this->error_array['description']);
    $this->assertEquals(serialize($this->error_array), serialize($error->toArray()));
  }

  public function testConversionToString()
  {
    $error = new Error($this->error_array['code'], $this->error_array['name'], $this->error_array['description']);
    $this->assertEquals(json_encode($this->error_array), (string) $error);
  }
}