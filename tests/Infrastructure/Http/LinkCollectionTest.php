<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-04-02
 * Time: 19:22
 */

namespace Tests\Infrastructure\Http;


use PHPUnit\Framework\TestCase;

class LinkCollectionTest extends TestCase
{
  public function testCreation()
  {
    $link = new \App\Infrastructure\Http\Link\LinkCollection();
    $this->assertInstanceOf(\App\Infrastructure\Http\Link\LinkCollection::class, $link);
  }
}