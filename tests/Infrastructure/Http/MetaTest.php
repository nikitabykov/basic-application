<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-04-02
 * Time: 19:26
 */

namespace Tests\Infrastructure\Http;


use App\Infrastructure\Http\Meta\Meta;
use PHPUnit\Framework\TestCase;

class MetaTest extends TestCase
{

  private $meta_array = [
    'name' => 'Meta Name',
    'value' => 'Meta Value'
  ];

  public function testCreation()
  {
    $meta = new Meta($this->meta_array['name'], $this->meta_array['value']);
    $this->assertInstanceOf(Meta::class, $meta);
  }

  public function testConversionToArray()
  {
    $meta = new Meta($this->meta_array['name'], $this->meta_array['value']);
    $this->assertEquals(
      serialize($this->meta_array),
      serialize($meta->toArray())
    );
  }

  public function testConversionToString()
  {
    $meta = new Meta($this->meta_array['name'], $this->meta_array['value']);
    $this->assertEquals(
      json_encode($this->meta_array),
      (string)$meta
    );
  }
}