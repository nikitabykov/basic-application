<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-04-02
 * Time: 19:26
 */

namespace Tests\Infrastructure\Http;


use App\Infrastructure\Http\Meta\MetaCollection;
use PHPUnit\Framework\TestCase;

class MetaCollectionTest extends TestCase
{
  public function testCreation()
  {
    $collection = new MetaCollection();
    $this->assertInstanceOf(MetaCollection::class, $collection);
  }
}