<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-04-02
 * Time: 11:41
 */

namespace Tests\Infrastructure\DataObject;


use App\Infrastructure\Http\Error\Error;
use App\Infrastructure\Http\Error\ErrorCollection;
use PHPUnit\Framework\TestCase;

class ErrorCollectionTest extends TestCase
{

  private $error_array = [
    'code' => 100,
    'name' => 'Test Name',
    'description' => 'Test Description'
  ];

  public function testCreation()
  {
    $collection = new ErrorCollection();
    $this->assertInstanceOf(ErrorCollection::class, $collection);
  }

  public function testErrorAdd()
  {

    $collection = new ErrorCollection();

    $error1 = new Error($this->error_array['code'], $this->error_array['name'], $this->error_array['description']);
    $collection->add($error1);
    $this->assertEquals(1, $collection->count());

    $error2 = new Error($this->error_array['code'], $this->error_array['name'], $this->error_array['description']);
    $collection->add($error2);
    $this->assertEquals(2, $collection->count());
  }

  public function testConversionToArray()
  {

    $collection = new ErrorCollection();
    $error = new Error($this->error_array['code'], $this->error_array['name'], $this->error_array['description']);

    $collection->add($error);

    $this->assertEquals(
      serialize([$this->error_array]),
      serialize($collection->toArray())
    );
  }

  public function testConversionToString()
  {
    $error = new Error($this->error_array['code'], $this->error_array['name'], $this->error_array['description']);
    $this->assertEquals(
      json_encode($this->error_array),
      (string) $error
    );
  }
}