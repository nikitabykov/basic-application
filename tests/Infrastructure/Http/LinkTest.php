<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-04-02
 * Time: 19:22
 */

namespace Tests\Infrastructure\Http;


use App\Infrastructure\Http\Link\Link;
use PHPUnit\Framework\TestCase;

class LinkTest extends TestCase
{
  public function testCreation()
  {
    $link = new Link('http://test.com');
    $this->assertInstanceOf(Link::class, $link);
  }
}