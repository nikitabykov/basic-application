<?php


namespace Tests\Domain;


class Utils
{

  private static $dictionaries = [
    ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',],
    ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',],
    [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
  ];

  private static function generatePasswordString(array $dictionaries, int $length = 7): string
  {
    $password = '';
    for ($i = 0; $i < $length; $i++) {
      $dictionary = next($dictionaries);
      if(!$dictionary)
        $dictionary = reset($dictionaries);
      $index = rand(0, count($dictionary) - 1);
      $password .= $dictionary[$index];
    }
    return $password;
  }

  public static function generateValidPasswordString(): string
  {
    return self::generatePasswordString(self::$dictionaries);
  }

  public static function generateInvalidPasswordStrings(): array
  {
    $passwords = [];
    $dictionaries = self::$dictionaries;

    // invalid length
    $passwords[] = self::generatePasswordString($dictionaries, 5);
    // 1-t invalid symbol collection
    $dictionaries_without_lowercase = $dictionaries;
    unset($dictionaries_without_lowercase[0]);
    $passwords[] = self::generatePasswordString($dictionaries_without_lowercase, 10);
    // 2-d invalid symbol collection
    $dictionaries_without_uppercase = $dictionaries;
    unset($dictionaries_without_uppercase[1]);
    $passwords[] = self::generatePasswordString($dictionaries_without_uppercase, 10);
    // 3-d invalid symbol collection
    $dictionaries_without_numbers = $dictionaries;
    unset($dictionaries_without_numbers[2]);
    $passwords[] = self::generatePasswordString($dictionaries_without_numbers, 10);

    return $passwords;
  }
}
