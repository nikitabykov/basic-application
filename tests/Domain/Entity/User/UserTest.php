<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-04-03
 * Time: 16:06
 */

namespace Tests\Domain\Entity\User;


use App\Domain\Entity\User\User;
use App\Domain\Entity\User\UserInterface;
use App\Domain\Entity\User\ValueObjects\Email\Email;
use App\Domain\Entity\User\ValueObjects\Password\Password;
use App\Domain\Entity\User\ValueObjects\Role\Role;
use App\Domain\Entity\User\ValueObjects\Role\RolesCollection;
use App\Domain\Entity\ValueObjects\Id\Id;
use PHPUnit\Framework\TestCase;
use Tests\Domain\Utils;

class UserTest extends TestCase
{

  private $user_data = [
    [
      'active' => true,
      'first_name' => 'Firstname',
      'last_name' => 'Lastname',
      'password' => '123password',
      'roles' => [
        [
          'code' => 'administrator',
          'name' => 'Administrator',
          'description' => 'Administrator role'
        ],
        [
          'code' => 'content_manager',
          'name' => 'Content manager',
          'description' => 'Content manager role'
        ]
      ]
    ],
    [
      'active' => true,
      'first_name' => 'Username',
      'last_name' => 'Userlastname',
      'password' => 'password123',
      'roles' => [
        [
          'code' => 'registered_user',
          'name' => 'User',
          'description' => 'User role'
        ],
        [
          'code' => 'content_manager',
          'name' => 'Content manager',
          'description' => 'Content manager role'
        ]
      ]
    ],
  ];

  public function testCreation()
  {

    $user_data = $this->getUserData();

    $roles = new RolesCollection();
    if(isset($user_data['roles'])) {
      foreach ($user_data['roles'] as $role) {
        $roles->add(Role::createFromState($role['code'], $role['name'], $role['description']));
      }
    }

    $user = new User(
      new Id(),
      isset($user_data['active']) ? $user_data['active'] : '',
      isset($user_data['first_name']) ? $user_data['first_name'] : '',
      isset($user_data['last_name']) ? $user_data['last_name'] : '',
      isset($user_data['email']) ? Email::createFromState($user_data['email']) : null,
      Password::createFromRaw(Utils::generateValidPasswordString()),
      $roles
    );

    $this->assertInstanceOf(UserInterface::class, $user);
  }

  private function getUserData()
  {
    shuffle($this->user_data);
    return $this->user_data[0];
  }
}
