<?php


namespace Tests\Domain\Entity\User\ValueObjects\Role;


use App\Domain\Entity\User\ValueObjects\Role\Role;
use App\Domain\Entity\User\ValueObjects\Role\RoleCodeFormatException;
use App\Domain\Entity\User\ValueObjects\Role\RoleInterface;
use PHPUnit\Framework\TestCase;

class RoleTest extends TestCase
{

  public function testCreation()
  {

    $role_state = [
      'code' => 'role_code',
      'name' => 'Role name',
      'description' => 'Role description'
    ];

    $role = Role::createFromState($role_state['code'], $role_state['name'], $role_state['description']);
    $role_array = $role->toArray();
    $this->assertInstanceOf(RoleInterface::class, $role);
    $this->assertEquals($role_state['code'], $role->getCode());
    $this->assertEquals($role_state['code'], $role_array['code']);
    $this->assertEquals($role_state['name'], $role_array['name']);
    $this->assertEquals($role_state['description'], $role_array['description']);

    $role = Role::createFromState($role_state['code'], $role_state['name']);
    $role_array = $role->toArray();
    $this->assertInstanceOf(RoleInterface::class, $role);
    $this->assertEquals($role_state['code'], $role->getCode());
    $this->assertEquals($role_state['code'], $role_array['code']);
    $this->assertEquals($role_state['name'], $role_array['name']);
    $this->assertEquals('', $role_array['description']);

    $role = Role::createFromState($role_state['code']);
    $role_array = $role->toArray();
    $this->assertInstanceOf(RoleInterface::class, $role);
    $this->assertEquals($role_state['code'], $role->getCode());
    $this->assertEquals($role_state['code'], $role_array['code']);
    $this->assertEquals('', $role_array['name']);
    $this->assertEquals('', $role_array['description']);
  }

  public function testFailCreationWithSpaces()
  {
    $this->expectException(RoleCodeFormatException::class);
    Role::createFromState('wrong code');
  }

  public function testFailCreationWithUppercase()
  {
    $this->expectException(RoleCodeFormatException::class);
    Role::createFromState('WrOnGcOdE');
  }

  public function testFailCreationWithSpecialSymbols()
  {
    $this->expectException(RoleCodeFormatException::class);
    Role::createFromState('!@#$%^');
  }

}
