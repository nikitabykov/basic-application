<?php


namespace Tests\Domain\Entity\User\ValueObjects\Role;


use App\Domain\Entity\User\ValueObjects\Role\Role;
use App\Domain\Entity\User\ValueObjects\Role\RolesCollection;
use PHPUnit\Framework\TestCase;

class RolesCollectionTest extends TestCase
{
  public function testCreation()
  {
    $collection = new RolesCollection();
    $this->assertInstanceOf(RolesCollection::class, $collection);
  }

  public function testAddNewRoleToCollection()
  {
    $role_array = $this->getRoleArray();
    $role = Role::createFromState($role_array['code'], $role_array['name'], $role_array['description']);
    $collection = new RolesCollection();
    $collection->add($role);
    $collection = $collection->toArray();
    $this->assertArrayHasKey($role_array['code'], $collection);
    $this->assertEquals($role_array['code'], $collection[$role_array['code']]['code']);
    $this->assertEquals($role_array['name'], $collection[$role_array['code']]['name']);
    $this->assertEquals($role_array['description'], $collection[$role_array['code']]['description']);
  }

  public function testAddManyRoles()
  {
    $roles = $this->generateRolesArray();
    $collection = new RolesCollection();
    foreach ($roles as $role) {
      $role = Role::createFromState($role['code']);
      $collection->add($role);
    }
    $this->assertEquals(count($roles), count($collection->toArray()));
  }

  public function testRemoveFromCollection()
  {
    $role_array_a = $this->getRoleArray();
    $role_array_b = $this->getRoleArray();

    $collection = new RolesCollection();
    $collection->add(Role::createFromState($role_array_a['code'], $role_array_a['name'], $role_array_a['description']));
    $collection->add(Role::createFromState($role_array_b['code'], $role_array_b['name'], $role_array_b['description']));

    $collection_before_array = $collection->toArray();
    $this->assertArrayHasKey($role_array_a['code'], $collection_before_array);
    $this->assertArrayHasKey($role_array_b['code'], $collection_before_array);
    $this->assertEquals(2, count($collection_before_array));

    $collection->remove($role_array_a['code']);
    $collection_after_array = $collection->toArray();
    $this->assertArrayHasKey($role_array_b['code'], $collection_after_array);
    $this->assertEquals(1, count($collection_after_array));
  }

  private function getRoleArray()
  {
    return [
      'code' => uniqid(),
      'name' => 'New role',
      'description' => 'Role description'
    ];
  }

  private function generateRolesArray()
  {
    $roles = [];
    for ($i = 0; $i < rand(1, 100); $i++) {
      $roles[] = $this->getRoleArray();
    }
    return $roles;
  }
}
