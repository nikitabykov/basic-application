<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-04-03
 * Time: 16:35
 */

namespace Tests\Domain\Entiity\User\ValueObjects\Password;

use App\Domain\Entity\User\ValueObjects\Password\Password;
use App\Domain\Entity\User\ValueObjects\Password\PasswordFormatException;
use PHPUnit\Framework\TestCase;
use Tests\Domain\Utils;

class PasswordTest extends TestCase
{
  public function testSuccessCreation()
  {
    $password_raw = Utils::generateValidPasswordString();

    $password_object = Password::createFromRaw($password_raw);
    $this->assertNotEmpty((string)$password_object);

    $password_from_state = Password::createFromState((string)$password_object);
    $this->assertEquals((string)$password_object, (string)$password_from_state);
  }

  public function testFailCreationWithoutLowercase()
  {
    $passwords = Utils::generateInvalidPasswordStrings();
    $this->expectException(PasswordFormatException::class);
    Password::createFromRaw($passwords[0]);
  }

  public function testFailCreationWithoutUppercase()
  {
    $passwords = Utils::generateInvalidPasswordStrings();
    $this->expectException(PasswordFormatException::class);
    Password::createFromRaw($passwords[1]);
  }


  public function testFailCreationWithoutNumbers()
  {
    $passwords = Utils::generateInvalidPasswordStrings();
    $this->expectException(PasswordFormatException::class);
    Password::createFromRaw($passwords[2]);
  }
}
