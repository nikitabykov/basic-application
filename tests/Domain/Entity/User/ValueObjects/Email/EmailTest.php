<?php


namespace Tests\Domain\Entity\User\ValueObjects\Email;

use App\Domain\Entity\User\ValueObjects\Email\Email;
use App\Domain\Entity\User\ValueObjects\Email\EmailFormatException;
use App\Domain\Entity\User\ValueObjects\Email\EmailInterface;
use PHPUnit\Framework\TestCase;

class EmailTest extends TestCase
{
  public function testCreation()
  {
    $email = Email::createFromState('somebody@somewhere.com');
    $this->assertInstanceOf(EmailInterface::class, $email);
  }

  public function testFailCreationWithSpecialSymbolsInName()
  {
    $this->expectException(EmailFormatException::class);
    Email::createFromState('$somebody@somewhere.com');
  }

  public function testFailCreationWithSpecialSymbolsInDomain()
  {
    $this->expectException(EmailFormatException::class);
    Email::createFromState('somebody@$somewhere.com');
  }

  public function testFailCreationWithSpecialSymbolsInFierstLevelDomain()
  {
    $this->expectException(EmailFormatException::class);
    Email::createFromState('somebody@somewhere.c$om');
  }

  public function testFailCreationWithSpaces()
  {
    $this->expectException(EmailFormatException::class);
    Email::createFromState('some body@somewhere.com');
  }

  public function testFailCreationWithShortDomain()
  {
    $this->expectException(EmailFormatException::class);
    Email::createFromState('some body@somewhere.c');
  }
}
