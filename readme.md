# About Basic (Sample) Application 

## Rules
1. **Domain-level doesn't know anything about Application- and Infrastructure-levels.** Domain-level contains entities, repository interfaces, events and use cases interfaces. Domain-levels provides interfaces which will be realized on Application-level.
2. **Application-level know's about Domain- and Infrastructure-levels.** Application-level realizes interfaces provided by Domain-level and provides interfaces which will be realized on Infrastructure-level.
3. **Infrastructure-level doesn't know anything about Domain-level, Infrastructure-level knows only interfaces provided by Application-level**. Infrastructure-level contains only basic realizations of DB-connections, Request- and Response-mechanisms and the same things, also it contains realization of interfaces provided by Application-level.

## Directory structure
- config
- public
- src
  - Application
  - Domain
  - Infrastructure
- static
- tests
- vendor

## Working order
Application is built as a collaboration of 2 layer models:

1. Variation of DDD oriented architecture with 3 layers: Application, Infrastructure, Domain.
2. Sub layers model on Application layer.
  
