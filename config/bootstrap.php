<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-02-18
 * Time: 13:25
 */

use App\Application\Controller\TokenController;
use App\Application\Controller\UserController;
use App\Application\Middleware\ApplicationMiddleware;
use Dotenv\Dotenv;
use Symfony\Bridge\PsrHttpMessage\Factory\DiactorosFactory;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Loader\YamlFileLoader;
use Symfony\Component\Routing\RequestContext;
use App\Application\Middleware\RoutingMiddleware;
use App\Application\Middleware\TokenMiddleware;
use App\Application\Middleware\PermissionsMiddleware;
use App\Application\Middleware\ControllerMiddleware;
use App\Application\DataObject\DataObject;
use Symfony\Component\Routing\Router;

/* requiring packages */
require_once __DIR__ . '/../vendor/autoload.php';

/* loading .env variables */
Dotenv::create(__DIR__ . '/../')->load();

/* preparing DI-container */
$container = new ContainerBuilder();

/* common */
$container->register('DataObject', DataObject::class);
$container->register(Request::class)
  ->setFactory([Request::class, 'createFromGlobals']);

/* MongoDB connection */
$container->register(\MongoDB\Driver\Manager::class, \MongoDB\Driver\Manager::class)
  ->addArgument('mongodb://' . getenv('MONGO_DB_USER') . ':' . getenv('MONGO_DB_PASSWORD') . '@' . getenv('MONGO_DB_HOST') . ':' . getenv('MONGO_DB_PORT').'/?authSource='.getenv('MONGO_DB_NAME'));

/* psr request */
$container->register(DiactorosFactory::class, DiactorosFactory::class);
$container->register('request', \Psr\Http\Message\ServerRequestInterface::class)
  ->setFactory([new Reference(DiactorosFactory::class), 'createRequest'])
  ->addArgument(new Reference(Request::class));

/* middlewares */
$container->register('ApplicationMiddleware', ApplicationMiddleware::class)
  ->addArgument(new Reference('RoutingMiddleware'))
  ->addArgument($container);
$container->register('RoutingMiddleware', RoutingMiddleware::class)
  ->addArgument(new Reference('TokenMiddleware'))
  ->addArgument($container);
$container->register('TokenMiddleware', TokenMiddleware::class)
  ->addArgument(new Reference('PermissionsMiddleware'))
  ->addArgument($container);
$container->register('PermissionsMiddleware', PermissionsMiddleware::class)
  ->addArgument(new Reference('ControllerMiddleware'))
  ->addArgument($container);
$container->register('ControllerMiddleware', ControllerMiddleware::class)
  ->addArgument(null)
  ->addArgument($container);

/* symfony routing */
$container->register(FileLocator::class, FileLocator::class)
  ->addArgument([realpath(getenv('APPLICATION_ROOT').getenv('ROUTES_CONFIG_DIR'))]);
$container->register(YamlFileLoader::class, YamlFileLoader::class)
  ->addArgument(new Reference(FileLocator::class));
$container->register(RequestContext::class, RequestContext::class)
  ->addMethodCall('fromRequest', [new Reference(Request::class)]);

$container->register('Router', Router::class)
  ->addArgument(new Reference(YamlFileLoader::class))
  ->addArgument('routes.yaml')
  ->addArgument(['cache_dir' => realpath(getenv('APPLICATION_ROOT').getenv('ROUTES_CACHE_DIR'))])
  ->addArgument(new Reference(RequestContext::class));

/* controllers */
$container->register('UserController', UserController::class)
  ->addArgument(new Reference('request'))
  ->addArgument($container);
$container->register('TokenController', TokenController::class)
  ->addArgument(new Reference('request'))
  ->addArgument($container);

/* @todo change full class names to short aliases */
