<?php

require_once __DIR__ . '/../config/bootstrap.php';

$Application = $container->get('ApplicationMiddleware');
$Application->handle($container->get('request'));
