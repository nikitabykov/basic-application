<?php


namespace App\Tools\Migrations\Core;


interface MigrationInterface
{
  public function up();

  public function down();
}
