<?php


namespace App\Tools\Migrations\Core;


interface Migrator
{
  public static function run();
}
