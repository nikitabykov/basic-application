<?php


namespace App\Tools\Migrations\Core;


interface Generator
{
  public static function create(string $prefix = '', string $suffix = ''): string;
}
