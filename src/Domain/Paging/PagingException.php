<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-04-09
 * Time: 10:46
 */

namespace App\Domain\Paging;


use App\Domain\DomainException;

class PagingException extends DomainException
{

}