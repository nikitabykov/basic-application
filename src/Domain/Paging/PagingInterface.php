<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-04-09
 * Time: 10:44
 */

namespace App\Domain\Paging;


interface PagingInterface
{
  public static function create(int $offset = 0, int $size = 20);

  public function getOffset() : int;

  public function getSize() : int;

  public function getNext() : PagingInterface;

  public function getPrevious() : PagingInterface;
}