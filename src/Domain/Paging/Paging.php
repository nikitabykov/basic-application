<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-04-09
 * Time: 10:45
 */

namespace App\Domain\Paging;


class Paging implements PagingInterface
{

  private $offset;
  private $size;

  private function __construct(int $offset, int $size)
  {
    $this->offset = $offset;
    $this->size = $size;
  }

  public static function create(int $offset = 0, int $size = 20)
  {
    if ($offset < 0)
      throw new PagingException("Offset should be grater or equal to 0, {$offset} was given");
    if ($size < 1)
      throw new PagingException("Size should be grater than 0, {$size} was given");

    return new self($offset, $size);
  }

  public function getOffset(): int
  {
    return $this->offset;
  }

  public function getSize(): int
  {
    return $this->size;
  }

  public function getNext(): PagingInterface
  {
    $size = $this->getSize();
    $offset = $this->getOffset() + $size;

    return new static($offset, $size);
  }

  public function getPrevious(): PagingInterface
  {
    $size = $this->getSize();
    $offset = $this->getOffset() - $size;

    return new static($offset, $size);
  }
}