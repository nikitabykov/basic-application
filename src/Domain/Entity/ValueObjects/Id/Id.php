<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-04-05
 * Time: 11:50
 */

namespace App\Domain\Entity\ValueObjects\Id;


class Id implements IdInterface
{

  private $id = null;

  public function __construct(string $id = null)
  {
    if ($id === null) {
      if (function_exists('com_create_guid') === true) {
        $this->id = trim(com_create_guid(), '{}');
      }

      $this->id = sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
    } else {
      $this->id = $id;
    }
  }

  public function __toString()
  {
    return $this->id;
  }

  public static function createFromState(string $id): IdInterface
  {
    if (!preg_match('/^\{?[A-Z0-9]{8}-[A-Z0-9]{4}-[A-Z0-9]{4}-[A-Z0-9]{4}-[A-Z0-9]{12}\}?$/', $id)) {
      throw new IdFormatException("Id '{$id}' was given in wrong format. Guid format required.");
    }
    return new self($id);
  }
}