<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-04-05
 * Time: 14:29
 */

namespace App\Domain\Entity\ValueObjects\Id;


use App\Domain\Entity\User\UserException;

class IdException extends UserException
{

}