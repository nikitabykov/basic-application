<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-04-05
 * Time: 11:50
 */

namespace App\Domain\Entity\ValueObjects\Id;


interface IdInterface
{
  public function __construct(string $id = null);

  public function __toString();

  public static function createFromState(string $id): IdInterface;
}