<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-04-03
 * Time: 11:20
 */

namespace App\Domain\Entity\User;


use App\Domain\Entity\EntityException;

class UserException extends EntityException
{

}