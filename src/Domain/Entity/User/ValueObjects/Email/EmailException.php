<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-04-03
 * Time: 16:28
 */

namespace App\Domain\Entity\User\ValueObjects\Email;


use App\Domain\Entity\User\UserException;

class EmailException extends UserException
{

}