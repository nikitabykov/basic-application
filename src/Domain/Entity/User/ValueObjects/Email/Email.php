<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-04-03
 * Time: 16:28
 */

namespace App\Domain\Entity\User\ValueObjects\Email;


class Email implements EmailInterface
{

  private $email = '';

  private function __construct(string $email)
  {
    $this->email = $email;
  }

  public function __toString()
  {
    return $this->email;
  }

  public static function createFromState(string $email): EmailInterface
  {

    $email = trim($email);

    if(!preg_match('/^[a-zA-Z0-9\-\_\.]+@[a-zA-Z0-9\-\_]+(\.[a-zA-Z]{2,})+$/', $email)) {
      throw new EmailFormatException("Email '{$email}' was given in wrong format.");
    }

    return new self($email);
  }
}
