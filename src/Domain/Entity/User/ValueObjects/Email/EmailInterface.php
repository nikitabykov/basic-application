<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-04-03
 * Time: 16:28
 */

namespace App\Domain\Entity\User\ValueObjects\Email;


interface EmailInterface
{
  public static function createFromState(string $email): EmailInterface;

  public function __toString();
}