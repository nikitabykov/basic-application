<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-04-03
 * Time: 10:20
 */

namespace App\Domain\Entity\User\ValueObjects\Password;


class Password implements PasswordInterface
{

  private $password_hash = null;

  private function __construct(string $password_hash)
  {
    $this->password_hash = $password_hash;
  }

  public function __toString(): string
  {
    return $this->password_hash;
  }

  public static function createFromState(string $password_hash): PasswordInterface
  {
    return new self($password_hash);
  }

  public static function createFromRaw(string $password): PasswordInterface
  {
    if(!preg_match('/(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{7,}$/', $password)) {
      throw new PasswordFormatException('Password should contain at least: one digit, one symbol in uppercase, one symbol in lowercase and shouldn\'t be shorter than 7 symbols.');
    }

    $password_hash = md5($password);
    return new self($password_hash);
  }
}