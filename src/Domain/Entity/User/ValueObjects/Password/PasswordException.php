<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-04-03
 * Time: 11:20
 */

namespace App\Domain\Entity\User\ValueObjects\Password;


use App\Domain\Entity\User\UserException;

class PasswordException extends UserException
{

}