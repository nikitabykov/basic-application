<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-04-03
 * Time: 11:21
 */

namespace App\Domain\Entity\User\ValueObjects\Password;

class PasswordFormatException extends PasswordException
{
}