<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-04-03
 * Time: 10:20
 */

namespace App\Domain\Entity\User\ValueObjects\Password;


interface PasswordInterface
{
  public function __toString(): string;

  public static function createFromState(string $password_hash): PasswordInterface;

  public static function createFromRaw(string $password): PasswordInterface;
}