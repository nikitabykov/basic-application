<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-04-05
 * Time: 16:28
 */

namespace App\Domain\Entity\User\ValueObjects\Role;


interface RolesCollectionInterface
{
  public function add(RoleInterface $role): RolesCollectionInterface;

  public function remove(string $role_code): RolesCollectionInterface;

  public function toArray(): array;
}