<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-04-05
 * Time: 16:28
 */

namespace App\Domain\Entity\User\ValueObjects\Role;


class Role implements RoleInterface
{

  private $role_code;
  private $role_name;
  private $role_description;

  private function __construct(string $role_code, string $role_name = '', string $role_description = '')
  {
    $this->role_code = $role_code;
    $this->role_name = $role_name;
    $this->role_description = $role_description;
  }

  public static function createFromState(string $role_code, string $role_name = '', string $role_description = ''): RoleInterface
  {
    if (!preg_match('/^[a-z0-9\_]+$/', $role_code)) {
      throw new RoleCodeFormatException("Role code should contain only letters in lowercase, digits and underscore symbol. '{$role_code}' was given.");
    }
    return new self($role_code, $role_name, $role_description);
  }

  public function getCode(): string
  {
    return $this->role_code;
  }

  public function toArray(): array
  {
    return [
      'code' => $this->role_code,
      'name' => $this->role_name,
      'description' => $this->role_description
    ];
  }
}