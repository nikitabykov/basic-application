<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-04-05
 * Time: 16:28
 */

namespace App\Domain\Entity\User\ValueObjects\Role;


interface RoleInterface
{
  public static function createFromState(string $role_code, string $role_name = '', string $role_description = ''): RoleInterface;

  public function getCode(): string;

  public function toArray(): array;
}