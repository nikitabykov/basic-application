<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-04-05
 * Time: 16:29
 */

namespace App\Domain\Entity\User\ValueObjects\Role;


use App\Domain\Entity\User\UserException;

class RoleException extends UserException
{

}