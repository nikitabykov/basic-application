<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-04-05
 * Time: 16:28
 */

namespace App\Domain\Entity\User\ValueObjects\Role;


class RolesCollection implements RolesCollectionInterface
{

  private $roles = [];

  public function add(RoleInterface $role): RolesCollectionInterface
  {
    $this->roles[$role->getCode()] = $role;
    return $this;
  }

  public function remove(string $role_code): RolesCollectionInterface
  {
    if (isset($this->roles[$role_code]))
      unset($this->roles[$role_code]);
    return $this;
  }

  public function toArray(): array
  {
    $roles = [];
    foreach ($this->roles as $role) {
      $roles[$role->getCode()] = $role->toArray();
    }
    return $roles;
  }
}