<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-04-02
 * Time: 21:59
 */

namespace App\Domain\Entity\User;

use App\Domain\Entity\EntityInterface;
use App\Domain\Entity\User\ValueObjects\Email\EmailInterface;
use App\Domain\Entity\User\ValueObjects\PersonalSecret\PersonalSecretInterface;
use App\Domain\Entity\ValueObjects\Id\IdInterface;
use App\Domain\Entity\User\ValueObjects\Password\PasswordInterface;
use App\Domain\Entity\User\ValueObjects\Role\RoleInterface;
use App\Domain\Entity\User\ValueObjects\Role\RolesCollectionInterface;

interface UserInterface extends EntityInterface
{
  public function __construct(
    IdInterface $unique_id,
    bool $active = true,
    string $first_name = '',
    string $last_name = '',
    EmailInterface $email = null,
    PasswordInterface $password = null,
    RolesCollectionInterface $roles = null
  );

  public static function createFromState(
    string $unique_id = '',
    bool $active = true,
    string $first_name = '',
    string $last_name = '',
    string $email = '',
    string $password = '',
    array $roles = []
  ): UserInterface;

  public function getUniqueId(): string;

  public function activate(): UserInterface;

  public function deactivate(): UserInterface;

  public function setFirstName(string $first_name): UserInterface;

  public function setLastName(string $last_name): UserInterface;

  public function setPassword(string $password): UserInterface;

  public function addRole(RoleInterface $role): UserInterface;

  public function removeRole(string $role_code): UserInterface;
}
