<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-04-07
 * Time: 16:03
 */

namespace App\Domain\Entity\User\Events;


use App\Domain\Event\AbstractEvent;

class UserCreatedEvent extends AbstractEvent
{

}