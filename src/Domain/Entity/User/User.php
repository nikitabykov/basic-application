<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-04-02
 * Time: 21:59
 */

namespace App\Domain\Entity\User;

use App\Domain\Entity\User\ValueObjects\Email\Email;
use App\Domain\Entity\User\ValueObjects\Email\EmailInterface;
use App\Domain\Entity\ValueObjects\Id\Id;
use App\Domain\Entity\ValueObjects\Id\IdInterface;
use App\Domain\Entity\User\ValueObjects\Password\Password;
use App\Domain\Entity\User\ValueObjects\Password\PasswordInterface;
use App\Domain\Entity\User\ValueObjects\Role\Role;
use App\Domain\Entity\User\ValueObjects\Role\RoleInterface;
use App\Domain\Entity\User\ValueObjects\Role\RolesCollection;
use App\Domain\Entity\User\ValueObjects\Role\RolesCollectionInterface;

class User implements UserInterface
{
  private $unique_id;
  private $active = true;
  private $first_name = '';
  private $last_name = '';
  private $email = null;
  private $password = null;
  private $roles = null;

  /**
   * User constructor.
   * @param IdInterface $unique_id
   * @param bool $active
   * @param string $first_name
   * @param string $last_name
   * @param EmailInterface|null $email
   * @param PasswordInterface|null $password
   * @param RolesCollectionInterface|null $roles
   */
  public function __construct(
    IdInterface $unique_id,
    bool $active = true,
    string $first_name = '',
    string $last_name = '',
    EmailInterface $email = null,
    PasswordInterface $password = null,
    RolesCollectionInterface $roles = null
  )
  {

    $this->unique_id = $unique_id;

    if ($active === true)
      $this->activate();
    else
      $this->deactivate();

    $this->setFirstName($first_name);
    $this->setLastName($last_name);

    if ($email !== null)
      $this->email = $email;

    $this->password = $password;

    if ($roles === null)
      $this->roles = new RolesCollection();
    else
      $this->roles = $roles;
  }

  public static function createFromState(
    string $unique_id = '',
    bool $active = true,
    string $first_name = '',
    string $last_name = '',
    string $email = '',
    string $password = '',
    array $roles = []
  ): UserInterface
  {
    $unique_id = Id::createFromState($unique_id);
    $email = Email::createFromState($email);
    $password = Password::createFromState($password);

    $role_collection = new RolesCollection();
    foreach ($roles as $role) {

      $role_code = isset($role['code']) ? (string)$role['code'] : null;
      $role_name = isset($role['name']) ? (string)$role['name'] : '';
      $role_description = isset($role['description']) ? (string)$role['description'] : '';

      $role_collection->add(Role::createFromState($role_code, $role_name, $role_description));
    }

    return new self(
      $unique_id,
      $active,
      $first_name,
      $last_name,
      $email,
      $password,
      $role_collection
    );
  }

  public function getUniqueId(): string
  {
    return (string)$this->unique_id;
  }

  public function activate(): UserInterface
  {
    $this->active = true;
    return $this;
  }

  public function deactivate(): UserInterface
  {
    $this->active = false;
    return $this;
  }

  public function setFirstName(string $first_name): UserInterface
  {
    $this->first_name = $first_name;
    return $this;
  }

  public function setLastName(string $last_name): UserInterface
  {
    $this->last_name = $last_name;
    return $this;
  }

  public function setPassword(string $password): UserInterface
  {
    $this->password = Password::createFromString($password);
    return $this;
  }

  public function addRole(RoleInterface $role): UserInterface
  {
    $this->roles->add($role);
    return $this;
  }

  public function removeRole(string $role_code): UserInterface
  {
    $this->roles->remove($role_code);
    return $this;
  }

  public function toArray(): array
  {
    return [
      'unique_id' => (string)$this->unique_id,
      'active' => $this->active,
      'first_name' => $this->first_name,
      'last_name' => $this->last_name,
      'email' => (string)$this->email,
      'password' => (string)$this->password,
      'roles' => $this->roles->toArray()
    ];
  }
}
