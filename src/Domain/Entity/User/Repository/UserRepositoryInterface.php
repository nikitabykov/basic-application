<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-04-05
 * Time: 16:04
 */

namespace App\Domain\Entity\User\Repository;


use App\Domain\Entity\User\UserInterface;
use App\Domain\Repository\RepositoryInterface;

interface UserRepositoryInterface extends RepositoryInterface
{
  public function getByEmail(string $email): ?UserInterface;

  public function getByUniqueId(string $unique_id): ?UserInterface;

  public function getByGroup(string $group_code): array;

  public function getByStatus(bool $active): array;
}