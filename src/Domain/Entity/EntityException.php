<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-04-08
 * Time: 11:33
 */

namespace App\Domain\Entity;


use App\Domain\DomainException;

class EntityException extends DomainException
{

}