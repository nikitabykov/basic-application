<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-04-07
 * Time: 15:24
 */

namespace App\Domain\Entity;


interface EntityInterface
{
  public function toArray(): array;
}