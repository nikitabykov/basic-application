<?php


namespace App\Domain\UseCases;


use App\Domain\Repository\RepositoryInterface;

interface UseCaseInterface
{
  public function __construct(?RepositoryInterface $repository);

  public function run(?array $parameters): ?array;
}
