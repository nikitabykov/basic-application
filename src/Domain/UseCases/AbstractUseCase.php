<?php


namespace App\Domain\UseCases;


use App\Domain\Repository\RepositoryInterface;

abstract class AbstractUseCase implements UseCaseInterface
{

  protected $repository;

  public function __construct(?RepositoryInterface $repository)
  {
    $this->repository = $repository ?? null;
  }
}
