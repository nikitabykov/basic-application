<?php


namespace App\Domain\UseCases;


use App\Domain\Entity\User\User;
use App\Domain\Entity\User\ValueObjects\Email\Email;
use App\Domain\Entity\User\ValueObjects\Password\Password;
use App\Domain\Entity\User\ValueObjects\Role\Role;
use App\Domain\Entity\User\ValueObjects\Role\RolesCollection;
use App\Domain\Entity\ValueObjects\Id\Id;

class CreateUserUseCase extends AbstractUseCase
{
  public function run(?array $parameters): ?array
  {
    $roles = new RolesCollection();
    if (isset($parameters['roles'])) {
      foreach ($parameters['roles'] as $role) {
        $roles->add(Role::createFromState(
          $role['code'],
          (isset($role['name']) ? $role['name'] : ''),
          (isset($role['description']) ? $role['description'] : '')
        ));
      }
    }

    $user = new User(
      new Id(),
      isset($parameters['active']) ? boolval($parameters['active']) : false,
      isset($parameters['first_name']) ? $parameters['first_name'] : '',
      isset($parameters['last_name']) ? $parameters['last_name'] : '',
      isset($parameters['email']) ? Email::createFromState($parameters['email']) : null,
      isset($parameters['password']) ? Password::createFromRaw($parameters['password']) : null,
      $roles
    );

    $this->repository->save($user);

    return $user->toArray();
  }
}
