<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-04-07
 * Time: 15:56
 */

namespace App\Domain\Event;


use App\Domain\Entity\EntityInterface;

abstract class AbstractEvent implements EventInterface
{

  protected $message;
  protected $entity;

  public function __construct(string $message, ?EntityInterface $entity)
  {
    $this->message = $message;
    if(is_object($entity))
      $this->entity = clone $entity;
  }

  public function toArray(): array
  {
    return $this->entity->toArray();
  }

  public function getMessage(): string
  {
    return $this->message;
  }
}