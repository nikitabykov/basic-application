<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-04-07
 * Time: 15:16
 */

namespace App\Domain\Event;


use App\Domain\Entity\EntityInterface;

interface EventInterface
{
  public function __construct(string $message, ?EntityInterface $entity);

  public function toArray(): array;

  public function getMessage(): string;
}