<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-04-07
 * Time: 17:44
 */

namespace App\Domain\Repository;


use App\Domain\Entity\EntityInterface;

interface RepositoryInterface
{
  public function save(EntityInterface $entity);
}