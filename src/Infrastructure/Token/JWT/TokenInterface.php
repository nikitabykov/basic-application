<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-02-22
 * Time: 18:05
 */

namespace App\Infrastructure\Token\JWT;


interface TokenInterface
{
  public function __construct(string $secret, string $algorithm = 'sha256', array $payload = []);

  public function getHeader(): array;

  public function getPayload(): array;

  public function getToken(): string;

  public function encode(): TokenDTOInterface;

  public static function decode(string $token, string $secret): TokenDTOInterface;
}