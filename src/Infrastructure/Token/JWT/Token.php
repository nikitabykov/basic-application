<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-02-22
 * Time: 15:28
 */

namespace App\Infrastructure\Token\JWT;


class Token implements TokenInterface
{

  private $secret = null;
  private $header = [
    'typ' => 'JWT',
    'alg' => null
  ];
  private $payload = [];

  private $token = null;

  public function __construct(string $secret, string $algorithm = 'sha256', array $payload = [])
  {

    $secret = trim($secret);

    if ($secret === '')
      throw new TokenEmptySecretException("Secret can't be empty");
    $this->secret = $secret;

    if (!in_array($algorithm, hash_hmac_algos()))
      throw new TokenAlgorithmException("Algorithm '{$algorithm}' not found, next algorithms are allowed: " . implode(', ', hash_hmac_algos()));

    $this->header['alg'] = $algorithm;

    $this->payload = array_merge(['expire_date' => (new \DateTimeImmutable())->format('U') + 30 * 24 * 60 * 60], $payload);

    $token = base64_encode(json_encode($this->header));
    $token = $token . '.' . base64_encode(json_encode($payload));
    $token = $token . '.' . hash_hmac($algorithm, $token, $secret);
    $token = 'Bearer ' . $token;

    $this->token = $token;
  }

  public function getHeader(): array
  {
    return $this->header;
  }

  public function getPayload(): array
  {
    return $this->payload;
  }

  public function getToken(): string
  {
    return $this->token;
  }

  public function encode(): TokenDTOInterface
  {
    return new TokenDTO($this);
  }

  public static function decode(string $token, string $secret): TokenDTOInterface
  {

    if (!preg_match_all('/^Bearer\s([a-zA-Z0-9\=\|])+\.([a-zA-Z0-9\=\|])+\.([a-zA-Z0-9\=\|])+/', $token))
      throw new TokenFormatIncorrectException("JWT token should start with 'Bearer ' and consist 3 parts separated by '.'");

    $destructured_token = explode('.', preg_replace('/^Bearer\s/', '', $token));

    $header = json_decode(base64_decode($destructured_token[0]), true);
    $payload = json_decode(base64_decode($destructured_token[1]), true);
    $algorithm = $header['alg'];

    $new_token = new self($secret, $algorithm, $payload);
    $new_token_state = $new_token->encode();

    if ($token !== $new_token_state->getToken()) {
      throw new TokenMismatchException("Given token ({$token}) doesn't match with calculated token {$new_token_state->getToken()}");
    }

    return $new_token_state;
  }
}