<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-02-22
 * Time: 17:58
 */

namespace App\Infrastructure\Token\JWT;


use App\Infrastructure\InfrastructureException;

class TokenException extends InfrastructureException
{

}