<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-02-22
 * Time: 18:01
 */

namespace App\Infrastructure\Token\JWT;


class TokenMismatchException extends TokenException
{

}