<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-02-22
 * Time: 15:28
 */

namespace App\Infrastructure\Token\JWT;


class TokenDTO implements TokenDTOInterface
{

  private $token;
  private $header;
  private $payload;

  public function __construct(Token $token)
  {
    $this->token = $token->getToken();
    $this->header = $token->getHeader();
    $this->payload = $token->getPayload();
  }

  public function getToken(): string
  {
    return $this->token;
  }

  public function getHeader(): array
  {
    return $this->header;
  }

  public function getPayload(): array
  {
    return $this->payload;
  }

  public function toArray(): array
  {
    return [
      'header' => $this->getHeader(),
      'payload' => $this->getPayload()
    ];
  }
}