<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-02-25
 * Time: 18:33
 */

namespace App\Infrastructure\Token\JWT;


class TokenFormatIncorrectException extends TokenException
{

}