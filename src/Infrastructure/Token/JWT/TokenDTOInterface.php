<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-02-22
 * Time: 18:05
 */

namespace App\Infrastructure\Token\JWT;


interface TokenDTOInterface
{
  public function __construct(Token $token);

  public function getToken(): string;

  public function getHeader(): array;

  public function getPayload(): array;

  public function toArray(): array;
}