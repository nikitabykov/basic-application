<?php


namespace App\Infrastructure\Logger\LoggerStorage;


use App\Infrastructure\Logger\Core\LoggerStorageInterface;
use App\Infrastructure\Logger\Core\LogMessageInterface;

/* @todo причесать */

class FileLogStorage implements LoggerStorageInterface
{

  private $log_dir = '/dev/null';

  public function __construct(string $log_dir)
  {
    $doc_root = realpath(__DIR__ . '/../../../../');
    $log_dir = $doc_root . '/' . $log_dir;
    $log_dir = self::preparePath($log_dir);
    if (!is_dir($log_dir)) {
      @mkdir($log_dir, 0777, true);
    }
    $this->log_dir = $log_dir;
  }

  public function write(LogMessageInterface $message)
  {
    $file = $this->log_dir . '/' . date('Y.m.d') . '.log';
    $file = self::preparePath($file);
    @file_put_contents($file, ((string)$message) . "\n", FILE_APPEND);
  }

  public static function preparePath(string $path)
  {
    return preg_replace(
      ['/\/+/', '/^\s*/', '/\s*$/', '/\/*$/'],
      ['/', '', '', ''],
      $path);
  }
}
