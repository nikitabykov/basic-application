<?php


namespace App\Infrastructure\Logger;


use App\Infrastructure\Logger\Core\LoggerCommonInterface;
use App\Infrastructure\Logger\Core\LoggerStorageInterface;
use Psr\Log\InvalidArgumentException;

class Logger implements LoggerCommonInterface
{

  const EMERGENCY_LEVEL = 'emergency';
  const ALERT_LEVEL = 'alert';
  const CRITICAL_LEVEL = 'critical';
  const ERROR_LEVEL = 'error';
  const WARNING_LEVEL = 'warning';
  const NOTICE_LEVEL = 'notice';
  const INFO_LEVEL = 'info';
  const DEBUG_LEVEL = 'debug';


  private $storage;

  public function __construct(LoggerStorageInterface $storage)
  {
    $this->storage = $storage;
  }

  /**
   * System is unusable.
   *
   * @param string $message
   * @param array $context
   *
   * @return void
   */
  public function emergency($message, array $context = array())
  {
    $this->log(self::EMERGENCY_LEVEL, $message, $context);
  }

  /**
   * Action must be taken immediately.
   *
   * Example: Entire website down, database unavailable, etc. This should
   * trigger the SMS alerts and wake you up.
   *
   * @param string $message
   * @param array $context
   *
   * @return void
   */
  public function alert($message, array $context = array())
  {
    $this->log(self::ALERT_LEVEL, $message, $context);
  }

  /**
   * Critical conditions.
   *
   * Example: Application component unavailable, unexpected exception.
   *
   * @param string $message
   * @param array $context
   *
   * @return void
   */
  public function critical($message, array $context = array())
  {
    $this->log(self::CRITICAL_LEVEL, $message, $context);
  }

  /**
   * Runtime errors that do not require immediate action but should typically
   * be logged and monitored.
   *
   * @param string $message
   * @param array $context
   *
   * @return void
   */
  public function error($message, array $context = array())
  {
    $this->log(self::ERROR_LEVEL, $message, $context);
  }

  /**
   * Exceptional occurrences that are not errors.
   *
   * Example: Use of deprecated APIs, poor use of an API, undesirable things
   * that are not necessarily wrong.
   *
   * @param string $message
   * @param array $context
   *
   * @return void
   */
  public function warning($message, array $context = array())
  {
    $this->log(self::WARNING_LEVEL, $message, $context);
  }

  /**
   * Normal but significant events.
   *
   * @param string $message
   * @param array $context
   *
   * @return void
   */
  public function notice($message, array $context = array())
  {
    $this->log(self::NOTICE_LEVEL, $message, $context);
  }

  /**
   * Interesting events.
   *
   * Example: User logs in, SQL logs.
   *
   * @param string $message
   * @param array $context
   *
   * @return void
   */
  public function info($message, array $context = array())
  {
    $this->log(self::INFO_LEVEL, $message, $context);
  }

  /**
   * Detailed debug information.
   *
   * @param string $message
   * @param array $context
   *
   * @return void
   */
  public function debug($message, array $context = array())
  {
    $this->log(self::DEBUG_LEVEL, $message, $context);
  }

  /**
   * Logs with an arbitrary level.
   *
   * @param mixed $level
   * @param string $message
   * @param array $context
   *
   * @return void
   */
  public function log($level, $message, array $context = array())
  {

    if (!in_array($level, [
      self::EMERGENCY_LEVEL,
      self::ALERT_LEVEL,
      self::CRITICAL_LEVEL,
      self::ERROR_LEVEL,
      self::WARNING_LEVEL,
      self::NOTICE_LEVEL,
      self::INFO_LEVEL,
      self::DEBUG_LEVEL
    ])) {
      throw new InvalidArgumentException();
    }

    $message = new LogMessage($level, $message, $context);
    $this->storage->write($message);
  }
}
