<?php


namespace App\Infrastructure\Logger\Core;


interface LoggerStorageInterface
{
  public function write(LogMessageInterface $message);
}
