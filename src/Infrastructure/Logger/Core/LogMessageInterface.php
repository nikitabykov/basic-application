<?php


namespace App\Infrastructure\Logger\Core;


interface LogMessageInterface
{
  public function __construct(string $level, string $message, array $context = []);

  public function getLevel(): string;

  public function getDateCreate(): string;

  public function getTimestamp(): string ;

  public function getMessage(): string;

  public function getContext(): array;

  public function __toString();
}
