<?php


namespace App\Infrastructure\Logger\Core;


use Psr\Log\LoggerInterface;

interface LoggerCommonInterface extends LoggerInterface
{
  public function __construct(LoggerStorageInterface $storage);
}
