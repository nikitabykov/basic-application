<?php


namespace App\Infrastructure\Logger;


use App\Infrastructure\Logger\Core\LogMessageInterface;

class LogMessage implements LogMessageInterface
{

  private $level;
  private $date_create;
  private $timestamp;
  private $message;
  private $context;

  public function __construct(string $level, string $message, array $context = [])
  {
    $this->level = $level;
    $this->date_create = (new \DateTimeImmutable())->format('d.m.Y H:i:s.u');
    $this->timestamp = (new \DateTimeImmutable())->format('U');
    $this->message = $message;
    $this->context = $context;
  }

  public function __toString()
  {
    return json_encode([
      'level' => $this->level,
      'date_create' => $this->date_create,
      'timestamp' => $this->timestamp,
      'message' => $this->message,
      'context' => $this->context
    ]);
  }

  public function getLevel(): string
  {
    return $this->level;
  }

  public function getMessage(): string
  {
    return $this->message;
  }

  public function getContext(): array
  {
    return $this->context;
  }

  public function getDateCreate(): string
  {
    return $this->date_create;
  }

  public function getTimestamp(): string
  {
    return $this->timestamp;
  }
}
