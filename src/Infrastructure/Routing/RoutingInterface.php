<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-02-20
 * Time: 16:05
 */

namespace App\Infrastructure\Routing;


use Psr\Http\Message\RequestInterface;

interface RoutingInterface
{
  public function getRoute() : ?Route;
}