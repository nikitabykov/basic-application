<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-02-21
 * Time: 11:49
 */

namespace App\Infrastructure\Routing;


class Route implements RouteInterface
{

  private $controller = null;
  private $controller_method = null;
  private $needs_auth = false;
  private $roles = [];
  private $variables = [];

  public function __construct(string $controller, string $controller_method, ?bool $needs_auth, ?array $roles, ?array $variables)
  {
    $this->controller = $controller;
    $this->controller_method = $controller_method;
    $this->needs_auth = $needs_auth === null ? false : $needs_auth;
    if ($roles !== null) {
      foreach ($roles as $role)
        $this->addRole($role);
    }
    $this->variables = $variables === null ? [] : $variables;
  }

  public function hasVariable(string $variable_name): bool
  {
    return isset($this->variables[$variable_name]);
  }

  public function getVariable(string $variable_name)
  {
    return (isset($this->variables[$variable_name])) ? $this->variables[$variable_name] : null;
  }

  public function getRoles(): array
  {
    return $this->roles;
  }

  public function getController(): string
  {
    return $this->controller;
  }

  public function getControllerMethod(): string
  {
    return $this->controller_method;
  }

  public function needsAuth(): bool
  {
    return $this->needs_auth;
  }

  private function addRole(string $role)
  {
    $this->roles[] = $role;
    $this->roles = array_unique($this->roles);
  }

  public function getVariables(): array
  {
    return $this->variables;
  }
}