<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-02-21
 * Time: 11:47
 */

namespace App\Infrastructure\Routing;


interface RouteInterface
{
  public function hasVariable(string $variable_name): bool;

  public function getVariable(string $variable_name);

  public function getVariables(): array;

  public function getRoles(): array;

  public function getController(): string;

  public function getControllerMethod(): string;

  public function needsAuth(): bool;
}