<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-04-01
 * Time: 18:45
 */

namespace App\Infrastructure\Http;


use App\Infrastructure\InfrastructureException;

class HttpResponseBodyException extends InfrastructureException
{

}