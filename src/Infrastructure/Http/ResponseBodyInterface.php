<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-04-01
 * Time: 17:52
 */

namespace App\Infrastructure\Http;


use App\Infrastructure\Http\Error\ErrorInterface;
use App\Infrastructure\Http\Link\LinkInterface;
use App\Infrastructure\Http\Meta\MetaInterface;

interface ResponseBodyInterface extends HttpCommonAccessInterface
{

  public function setData($data): ResponseBodyInterface;

  public function addError(ErrorInterface $error): ResponseBodyInterface;

  public function addLink(LinkInterface $link, string $type): ResponseBodyInterface;

  public function addMeta(MetaInterface $meta): ResponseBodyInterface;
}