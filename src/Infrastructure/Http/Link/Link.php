<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-04-01
 * Time: 18:10
 */

namespace App\Infrastructure\Http\Link;


class Link implements LinkInterface
{
  private $href = '';

  public function __construct(string $href)
  {
    $this->href = $href;
  }

  public function toArray(): array
  {
    return [$this->href];
  }

  public function __toString(): string
  {
    return $this->href;
  }
}