<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-04-01
 * Time: 17:54
 */

namespace App\Infrastructure\Http\Link;


use App\Infrastructure\Http\HttpCommonAccessInterface;

interface LinkInterface extends HttpCommonAccessInterface
{

}