<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-04-01
 * Time: 18:11
 */

namespace App\Infrastructure\Http\Link;


class LinkCollection implements LinkCollectionInterface
{

  private $self = null;
  private $next = null;
  private $previous = null;
  private $first = null;
  private $last = null;

  public function toArray(): array
  {
    $result = [];

    if($this->self !== null)
      $result['self'] = (string) $this->self;

    if($this->next !== null)
      $result['next'] = (string) $this->next;

    if($this->previous !== null)
      $result['previous'] = (string) $this->previous;

    if($this->first !== null)
      $result['first'] = (string) $this->first;

    if($this->last !== null)
      $result['last'] = (string) $this->last;

    return $result;
  }

  public function __toString(): string
  {
    return json_encode($this->toArray());
  }

  public function addSelf(LinkInterface $link): LinkCollectionInterface
  {
    $this->self = $link;
    return $this;
  }

  public function addNext(LinkInterface $link): LinkCollectionInterface
  {
    $this->next = $link;
    return $this;
  }

  public function addPrevious(LinkInterface $link): LinkCollectionInterface
  {
    $this->previous = $link;
    return $this;
  }

  public function addFirst(LinkInterface $link): LinkCollectionInterface
  {
    $this->first = $link;
    return $this;
  }

  public function addLast(LinkInterface $link): LinkCollectionInterface
  {
    $this->last = $link;
    return $this;
  }

  public function count(): int
  {
    $count = 0;

    if($this->self) $count++;
    if($this->next) $count++;
    if($this->previous) $count++;
    if($this->first) $count++;
    if($this->last) $count++;

    return $count;
  }
}