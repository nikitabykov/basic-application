<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-04-01
 * Time: 18:07
 */

namespace App\Infrastructure\Http\Link;


use App\Infrastructure\Http\HttpCommonAccessInterface;

interface LinkCollectionInterface extends HttpCommonAccessInterface
{
  public function addSelf(LinkInterface $link): LinkCollectionInterface;

  public function addNext(LinkInterface $link): LinkCollectionInterface;

  public function addPrevious(LinkInterface $link): LinkCollectionInterface;

  public function addFirst(LinkInterface $link): LinkCollectionInterface;

  public function addLast(LinkInterface $link): LinkCollectionInterface;

  public function count(): int;
}