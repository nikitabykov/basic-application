<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-04-01
 * Time: 18:06
 */

namespace App\Infrastructure\Http\Error;


use App\Infrastructure\Http\HttpCommonAccessInterface;

interface ErrorCollectionInterface extends HttpCommonAccessInterface
{
  public function add(ErrorInterface $error): ErrorCollectionInterface;

  public function count(): int;
}