<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-04-01
 * Time: 18:10
 */

namespace App\Infrastructure\Http\Error;


class Error implements ErrorInterface
{
  private $code;
  private $name;
  private $description;

  public function __construct(int $code = 0, string $name = '', string $description = '')
  {
    $this->code = $code;
    $this->name = $name;
    $this->description = $description;
  }

  public function toArray(): array
  {
    return [
      'code' => $this->code,
      'name' => $this->name,
      'description' => $this->description
    ];
  }

  public function __toString(): string
  {
    return json_encode($this->toArray());
  }
}