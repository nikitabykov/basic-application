<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-04-01
 * Time: 18:10
 */

namespace App\Infrastructure\Http\Error;


class ErrorCollection implements ErrorCollectionInterface
{

  private $errors = [];

  public function toArray(): array
  {
    $errors = $this->errors;
    foreach ($errors as &$error) {
      $error = $error->toArray();
    }
    return $errors;
  }

  public function __toString(): string
  {
    $errors = $this->toArray();
    $errors = json_encode($errors);

    return $errors;
  }

  public function add(ErrorInterface $error): ErrorCollectionInterface
  {
    $this->errors[] = $error;
    return $this;
  }

  public function count(): int
  {
    return count($this->errors);
  }
}