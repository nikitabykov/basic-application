<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-04-01
 * Time: 18:11
 */

namespace App\Infrastructure\Http\Meta;


class MetaCollection implements MetaCollectionInterface
{
  private $meta = [];

  public function toArray(): array
  {
    $meta_array = [];
    foreach ($this->meta as $meta_item) {
      $meta_item = $meta_item->toArray();
      $meta_array[$meta_item['name']] = $meta_item['value'];
    }
    return $meta_array;
  }

  public function __toString(): string
  {
    return json_encode($this->toArray());
  }

  public function add(MetaInterface $meta): MetaCollectionInterface
  {
    $this->meta[] = $meta;
    return $this;
  }

  public function count(): int
  {
    return count($this->meta);
  }
}