<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-04-01
 * Time: 17:54
 */

namespace App\Infrastructure\Http\Meta;


use App\Infrastructure\Http\HttpCommonAccessInterface;

interface MetaInterface extends HttpCommonAccessInterface
{

}