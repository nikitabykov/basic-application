<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-04-01
 * Time: 18:11
 */

namespace App\Infrastructure\Http\Meta;


class Meta implements MetaInterface
{
  private $name = '';
  private $value = '';

  public function __construct(string $name, string $value)
  {
    $this->name = $name;
    $this->value = $value;
  }

  public function toArray(): array
  {
    return [
      'name' => $this->name,
      'value' => $this->value
    ];
  }

  public function __toString(): string
  {
    return json_encode($this->toArray());
  }
}