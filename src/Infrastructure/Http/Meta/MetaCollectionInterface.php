<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-04-01
 * Time: 18:07
 */

namespace App\Infrastructure\Http\Meta;


use App\Infrastructure\Http\HttpCommonAccessInterface;

interface MetaCollectionInterface extends HttpCommonAccessInterface
{
  public function add(MetaInterface $meta): MetaCollectionInterface;

  public function count(): int;
}