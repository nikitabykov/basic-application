<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-04-01
 * Time: 18:05
 */

namespace App\Infrastructure\Http;


interface HttpCommonAccessInterface
{
  public function toArray(): array;

  public function __toString(): string;
}