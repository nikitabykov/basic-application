<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-04-01
 * Time: 17:49
 */

namespace App\Infrastructure\Http;

use App\Infrastructure\Http\Error\ErrorCollection;
use App\Infrastructure\Http\Error\ErrorInterface;
use App\Infrastructure\Http\Link\LinkCollection;
use App\Infrastructure\Http\Link\LinkInterface;
use App\Infrastructure\Http\Meta\Meta;
use App\Infrastructure\Http\Meta\MetaCollection;
use App\Infrastructure\Http\Meta\MetaInterface;

class ResponseBody implements ResponseBodyInterface
{

  private $meta;
  private $data = [];
  private $links;
  private $errors;

  public function __construct()
  {
    $this->meta = new MetaCollection();
    $this->meta->add(new Meta('Timestamp', (new \DateTimeImmutable())->format('d.m.Y H:i:s.v')));
    $this->links = new LinkCollection();
    $this->errors = new ErrorCollection();
  }

  public function setData($data): ResponseBodyInterface
  {
    $this->data = $data;
    return $this;
  }

  public function addError(ErrorInterface $error): ResponseBodyInterface
  {
    $this->errors->add($error);
    return $this;
  }

  public function addLink(LinkInterface $link, string $type): ResponseBodyInterface
  {

    switch ($type) {
      case 'self':
        $this->links->addSelf($link);
        break;

      case 'next':
        $this->links->addNext($link);
        break;

      case 'previous':
        $this->links->addPrevious($link);
        break;

      case 'first':
        $this->links->addFirst($link);
        break;

      case 'last':
        $this->links->addLast($link);
        break;

      default:
        throw new UnknownLinkTypeResponseBodyException("Unknown link type '{$type}' given. Link type should be in (self, next, previous, first, last)");
        break;
    }

    return $this;
  }

  public function addMeta(MetaInterface $meta): ResponseBodyInterface
  {
    $this->meta->add($meta);
    return $this;
  }

  public function toArray(): array
  {
    $array = [];

    if($this->meta->count() > 0) $array['meta'] = $this->meta->toArray();
    $array['data'] = $this->data;
    if($this->errors->count() > 0) $array['errors'] = $this->errors->toArray();
    if($this->links->count() > 0) $array['links'] = $this->links->toArray();

    return $array;
  }

  public function __toString(): string
  {
    return json_encode($this->toArray());
  }
}