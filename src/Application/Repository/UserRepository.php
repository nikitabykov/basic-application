<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-04-08
 * Time: 18:56
 */

namespace App\Application\Repository;


use App\Application\Core\Repository\AbstractRepository;
use App\Application\Storage\Filter\MongoFilter;
use App\Domain\Entity\EntityInterface;
use App\Domain\Entity\User\Events\UserCreatedEvent;
use App\Domain\Entity\User\Repository\UserRepositoryInterface;
use App\Domain\Entity\User\User;
use App\Domain\Entity\User\UserInterface;

class UserRepository extends AbstractRepository implements UserRepositoryInterface
{

  public function save(EntityInterface $entity)
  {
    $event = new UserCreatedEvent('User created', $entity);
    $this->notify($event);
  }

  public function getByEmail(string $email): ?UserInterface
  {
    $filter = new MongoFilter();
    $filter->equal('email', $email);
    $result = $this->storage->getByFilter($filter);
    foreach ($result as &$user) {
      $user = User::createFromState(
        $user->unique_id,
        $user->active,
        $user->first_name,
        $user->last_name,
        $user->email,
        $user->password,
        array_map(function ($role) {
          return (array)$role;
        }, (array)$user->roles)
      );
      return $user;
    }
    return null;
  }

  public function getByUniqueId(string $unique_id): ?UserInterface
  {

    $filter = new MongoFilter();
    $filter->equal('unique_id', $unique_id);

    $result = $this->storage->getByFilter($filter);

    if (count($result) > 0) {
      $data = current($result);

      return User::createFromState(
        $data->unique_id,
        $data->active,
        $data->first_name,
        $data->last_name,
        $data->email,
        $data->password,
        array_map(function ($role) {
          return (array)$role;
        }, (array)$data->roles)
      );
    }

    return null;
  }

  public function getByGroup(string $group_code): array
  {
    return [];
  }

  public function getByStatus(bool $active): array
  {
    $filter = new MongoFilter();
    $filter->equal('active', $active);
    $result = $this->storage->getByFilter($filter);
    foreach ($result as &$user) {
      $user = User::createFromState(
        $user->unique_id,
        $user->active,
        $user->first_name,
        $user->last_name,
        $user->email,
        $user->password,
        array_map(function ($role) {
          return (array)$role;
        }, (array)$user->roles)
      );
    }
    return $result;
  }
}
