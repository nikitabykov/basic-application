<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-04-09
 * Time: 10:25
 */

namespace App\Application\Core\Storage;


use App\Application\Core\Event\EventHandlerInterface;

interface StorageInterface extends EventHandlerInterface
{
    public function getByFilter(FilterInterface $filter, int $offset = 0, int $size = 0): array;
}