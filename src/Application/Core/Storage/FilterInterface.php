<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-04-09
 * Time: 10:42
 */

namespace App\Application\Core\Storage;


interface FilterInterface
{
  public function equal(string $key, $value): FilterInterface;

  public function notEqual(string $key, $value): FilterInterface;

  public function in(string $key, array $values): FilterInterface;

  public function notIn(string $key, array $values): FilterInterface;

  public function lower(string $key, $value): FilterInterface;

  public function greater(string $key, $value): FilterInterface;

  public function lowerOrEqual(string $key, $value): FilterInterface;

  public function greaterOrEqual(string $key, $value): FilterInterface;

  public function getFilter(): array;

  /**
   * @todo add logical groups like: (equal to ...) or (equal to ...), or: (equal to ... and grater than ...) and (lower than ...)
   */
}