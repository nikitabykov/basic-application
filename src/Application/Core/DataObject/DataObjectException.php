<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-04-08
 * Time: 18:06
 */

namespace App\Application\Core\DataObject;


use App\Application\ApplicationException;

class DataObjectException extends ApplicationException
{

}