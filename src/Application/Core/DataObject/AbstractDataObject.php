<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-04-08
 * Time: 18:04
 */

namespace App\Application\Core\DataObject;


class AbstractDataObject
{
  protected $variables = [];

  protected function setVariable(string $variable_name, $variable)
  {
    if (isset($this->variables[$variable_name]))
      throw new DataObjectException("Variable '{$variable_name}' already exists");

    $this->variables[$variable_name] = $variable;
  }

  protected function getVariable(string $variable_name)
  {
    if (!isset($this->variables[$variable_name]))
      throw new DataObjectException("Variable '{$variable_name}' wasn't found");

    return $this->variables[$variable_name];
  }
}