<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-02-28
 * Time: 12:15
 */

namespace App\Application\Core\Controller;


use App\Application\ApplicationException;

class ControllerException extends ApplicationException
{

}