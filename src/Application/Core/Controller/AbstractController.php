<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-02-28
 * Time: 12:02
 */

namespace App\Application\Core\Controller;

use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class AbstractController implements ControllerInterface
{

  private $request = null;
  private $container = null;

  public function __construct(ServerRequestInterface $request, ?ContainerInterface $container = null)
  {
    $this->request = $request;
    $this->container = $container;
  }

  public function callMethod(string $method_name, array $params = []): ?ResponseInterface
  {
    $reflection = new \ReflectionClass($this);

    if ($reflection->hasMethod($method_name) !== true) {
      throw new ControllerMethodMissedException("Method '{$method_name}' is missed in controller " . get_class($this));
    }

    $arguments = [];
    foreach ($reflection->getMethod($method_name)->getParameters() as $parameter) {
      $arguments[] = $this->getParamValue($parameter);
    }

    return call_user_func_array([$this, $method_name], $arguments);
  }

  private function getParamValue(\ReflectionParameter $parameter)
  {
    /*
     * сначала определяем - где будем искать:
     *   - объекты ищем в контейнере 2-я способами:
     *     - сначала по классу
     *     - потом по алиасу
     *   - базовые типы ищем в запросе и в роуте
     *   - без типа ищем везде по названию
     */

    $where_to_search = [
      'Request' => $this->request->getQueryParams()
    ];

    if ($this->container) {
      $where_to_search['Container'] = $this->container;
      if ($this->container->has('DataObject')) {
        $where_to_search['Route'] = $this->container->get('DataObject')->getRoute();
      }
    }

    $parameter_name = $parameter->getName();
    $parameter_type = $parameter->getType() ? $parameter->getType()->getName() : '';
    $parameter_is_optional = $parameter->isOptional();
    $parameter_default_value = $parameter->isDefaultValueAvailable() ? $parameter->getDefaultValue() : ($parameter->allowsNull() ? null : '');
    $parameter_suggested_value = null;

    // если у нас у параметра есть базовый тип, то мы его не будем контейнере, ищем только в роуте
    if (in_array($parameter_type, ['string', 'int', 'float', 'bool', 'array'])) {
      if (isset($where_to_search['Route']) && $where_to_search['Route']->hasVariable($parameter_name)) {
        $parameter_suggested_value = $where_to_search['Route']->getVariable($parameter_name);
      } elseif (isset($where_to_search['Request'][$parameter_name])) {
        $parameter_suggested_value = $where_to_search['Request'][$parameter_name];
      }
    } // если у параметра не задан тип - он может быть чем угодно и мы ищем его по имени в роуте а потом в контейнере
    elseif ($parameter_type === '') {
      if (isset($where_to_search['Route']) && $where_to_search['Route']->hasVariable($parameter_name)) {
        $parameter_suggested_value = $where_to_search['Route']->getVariable($parameter_name);
      } elseif (isset($where_to_search['Request'][$parameter_name])) {
        $parameter_suggested_value = $where_to_search['Request'][$parameter_name];
      } elseif (isset($where_to_search['Container']) && $where_to_search['Container']->has($parameter_name)) {
        $parameter_suggested_value = $where_to_search['Container']->get($parameter_name);
      }
    } // а это намекает нам на нестандартный тип, на на имя класса, а это может быть только в контейнере
    else {
      //  в контейнере в качестве ID может быть алиас или полное имя, потому ищем сначала по алиасу, если не находим - ищем по имени
      if (isset($where_to_search['Container']) && $where_to_search['Container']->has($parameter_name)) {
        $parameter_suggested_value = $where_to_search['Container']->get($parameter_name);
      } elseif (isset($where_to_search['Container']) && $where_to_search['Container']->has($parameter_type)) {
        $parameter_suggested_value = $where_to_search['Container']->get($parameter_type);
      }
    }

    switch ($parameter_type) {
      case 'string':
        $parameter_suggested_value = (string)$parameter_suggested_value;
        break;
      case 'int':
        $parameter_suggested_value = (int)$parameter_suggested_value;
        break;
      case 'float':
        $parameter_suggested_value = (float)$parameter_suggested_value;
        break;
      case 'bool':
        $parameter_suggested_value = (bool)$parameter_suggested_value;
        break;
      case 'array':
        $parameter_suggested_value = is_array($parameter_suggested_value) ?? null;
        break;
    }

    if ($parameter_suggested_value === null) {
      if ($parameter_default_value !== null) {
        $parameter_suggested_value = $parameter_default_value;
      } else if ($parameter_is_optional !== true) {
        throw new ControllerParameterValueException("Value for '{$parameter_name}' undefined or unreachable");
      }
    }

    return $parameter_suggested_value;
  }
}