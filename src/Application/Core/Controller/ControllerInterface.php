<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-02-28
 * Time: 12:02
 */

namespace App\Application\Core\Controller;


use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

interface ControllerInterface
{
  public function __construct(ServerRequestInterface $request, ?ContainerInterface $container = null);

  public function callMethod(string $method_name, array $params = []) : ?ResponseInterface;
}