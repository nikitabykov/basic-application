<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-02-15
 * Time: 18:35
 */

namespace App\Application\Core\Middleware;


use Psr\Container\ContainerInterface;;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

interface MiddlewareInterface
{

  public function __construct(MiddlewareInterface $middleware = null, ?ContainerInterface $container = null);

  public function handle(ServerRequestInterface $request): ?ResponseInterface;
}