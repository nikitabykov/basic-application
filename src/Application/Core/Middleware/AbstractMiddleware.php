<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-02-15
 * Time: 18:39
 */

namespace App\Application\Core\Middleware;


use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class AbstractMiddleware implements MiddlewareInterface
{

  protected $middleware = null;
  protected $container = null;

  public function __construct(MiddlewareInterface $middleware = null, ContainerInterface $container = null)
  {
    $this->middleware = $middleware;
    $this->container = $container;
  }

  public function handle(ServerRequestInterface $request): ?ResponseInterface
  {
    if ($this->middleware !== null) {
      return $this->middleware->handle($request);
    }
    return null;
  }
}