<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-04-08
 * Time: 17:56
 */

namespace App\Application\Core\Event;


use App\Domain\Event\EventInterface;

interface EventHandlerInterface
{
  public function handle(EventInterface $event);
}