<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-04-08
 * Time: 18:00
 */

namespace App\Application\Core\Event;


abstract class AbstractEventProducer implements EventProducerInterface
{

  protected $handlers = [];

  public function addHandler(EventHandlerInterface $handler): ?EventProducerInterface
  {
    $this->handlers[] = $handler;
  }
}