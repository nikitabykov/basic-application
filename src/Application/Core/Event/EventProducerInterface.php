<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-04-08
 * Time: 17:57
 */

namespace App\Application\Core\Event;


interface EventProducerInterface
{
  public function addHandler(EventHandlerInterface $handler): ?EventProducerInterface;
}