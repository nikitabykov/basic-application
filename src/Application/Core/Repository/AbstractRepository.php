<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-04-08
 * Time: 18:30
 */

namespace App\Application\Core\Repository;


use App\Application\Core\Event\AbstractEventProducer;
use App\Application\Core\Storage\StorageInterface;
use App\Domain\Event\EventInterface;

abstract class AbstractRepository extends AbstractEventProducer
{

  protected $storage;

  public function __construct(StorageInterface $storage)
  {
    $this->storage = $storage;
  }

  protected function notify(EventInterface $event)
  {
    $this->storage->handle($event);
    foreach ($this->handlers as $handler)
      $handler->handle($event);
  }
}