<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-02-21
 * Time: 18:15
 */

namespace App\Application\DataObject;


use App\Application\Core\DataObject\AbstractDataObject;
use App\Application\Core\DataObject\DataObjectException;
use App\Infrastructure\Routing\Route;
use App\Infrastructure\Token\JWT\TokenDTO;

class DataObject extends AbstractDataObject implements DataObjectInterface
{

  public function setRoute(Route $route): DataObjectInterface
  {
    try {
      $this->setVariable('route', $route);
      return $this;
    } catch (DataObjectException $exception) {
      throw new SetRouteDataObjectException($exception->getMessage());
    }
  }

  public function setToken(TokenDTO $token): DataObjectInterface
  {
    try {
      $this->setVariable('token', $token);
      return $this;
    } catch (DataObjectException $exception) {
      throw new SetTokenDataObjectException($exception->getMessage());
    }
  }

  public function getRoute(): ?Route
  {
    try {
      return $this->getVariable('route');
    } catch (DataObjectException $exception) {
      throw $exception;
    }
  }

  public function getToken(): ?TokenDTO
  {
    try {
      return $this->getVariable('token');
    } catch (DataObjectException $exception) {
      throw $exception;
    }
  }
}