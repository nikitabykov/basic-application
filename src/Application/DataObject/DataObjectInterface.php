<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-02-22
 * Time: 11:10
 */

namespace App\Application\DataObject;


use App\Infrastructure\Routing\Route;
use App\Infrastructure\Token\JWT\TokenDTO;

interface DataObjectInterface
{

  public function setRoute(Route $route): DataObjectInterface;

  public function getRoute(): ?Route;

  public function setToken(TokenDTO $token): DataObjectInterface;

  public function getToken(): ?TokenDTO;
}