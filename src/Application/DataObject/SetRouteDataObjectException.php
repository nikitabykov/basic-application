<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-02-21
 * Time: 18:18
 */

namespace App\Application\DataObject;


use App\Application\Core\DataObject\DataObjectException;

class SetRouteDataObjectException extends DataObjectException
{

}