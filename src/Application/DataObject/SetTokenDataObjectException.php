<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-02-25
 * Time: 19:02
 */

namespace App\Application\DataObject;

use App\Application\Core\DataObject\DataObjectException;

class SetTokenDataObjectException extends DataObjectException
{

}