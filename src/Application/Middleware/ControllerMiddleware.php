<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-02-18
 * Time: 11:36
 */

namespace App\Application\Middleware;

use App\Application\Core\Middleware\AbstractMiddleware;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class ControllerMiddleware extends AbstractMiddleware
{
  public function handle(ServerRequestInterface $request): ?ResponseInterface
  {
    $route = $this->container->get('DataObject')->getRoute();
    $controller = $this->container->get($route->getController());
    $response = $controller->callMethod($route->getControllerMethod(), []);

    return $response;
  }
}