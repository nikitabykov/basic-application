<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-04-10
 * Time: 17:47
 */

namespace App\Application\Middleware;


use App\Application\ApplicationException;
use App\Application\Core\Middleware\AbstractMiddleware;
use App\Infrastructure\Http\Error\Error;
use App\Infrastructure\Http\ResponseBody;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Symfony\Bridge\PsrHttpMessage\Factory\HttpFoundationFactory;
use Symfony\Component\HttpFoundation\Response;

class ApplicationMiddleware extends AbstractMiddleware
{

  public function handle(ServerRequestInterface $request): ?ResponseInterface
  {
    try {
      if ($response = parent::handle($request)) {
        $http_foundation_factory = new HttpFoundationFactory();
        $response = $http_foundation_factory->createResponse($response);
        $response->send();
      }
    } catch (ApplicationException $e) {
      // @todo log with different levels
    } catch (\Throwable $e) {
      // @todo log with different levels
      $response = new ResponseBody();
      $response->addError(new Error($e->getCode(), $e->getMessage(), $e->getTraceAsString()));
      $response = new Response($response, 500, ['Content-Type' => 'application/json']);
      $response->send();
    }

    return null;
  }
}
