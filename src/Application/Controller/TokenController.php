<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-04-01
 * Time: 14:38
 */

namespace App\Application\Controller;


use App\Application\Core\Controller\AbstractController;
use App\Application\Repository\UserRepository;
use App\Application\Storage\User\UserStorageMongoDB;
use App\Domain\Entity\User\User;
use App\Domain\Entity\User\ValueObjects\Password\Password;
use App\Infrastructure\Http\ResponseBody;
use App\Infrastructure\Token\JWT\Token;
use MongoDB\Driver\Manager;
use Psr\Http\Message\RequestInterface;
use Symfony\Bridge\PsrHttpMessage\Factory\DiactorosFactory;

class TokenController extends AbstractController
{
  public function create(RequestInterface $request, Manager $manager)
  {
    $result = [];
    $body = json_decode($request->getBody()->getContents(), true);

    $storage = new UserStorageMongoDB($manager, getenv('MONGO_DB_NAME'));
    $repository = new UserRepository($storage);
    $user = $repository->getByEmail($body['email']);
    if ($user) {
      $user = $user->toArray();
      $password = Password::createFromRaw($body['password']);
      $password = (string)$password;

      if ($user['password'] == $password) {
        $token = (new Token(getenv('TOKEN_SECRET'), 'sha256', [
          'unique_id' => $user['unique_id'],
          'email' => $user['email'],
          'roles' => $user['roles']
        ]))->encode()->getToken();

        $result = [
          'token' => $token
        ];
      } else {
        // @todo password missmatch
      }
    } else {
      // @todo user not found
    }

    $response_body = new ResponseBody();
    $response_body->setData($result);

    $response = new \Symfony\Component\HttpFoundation\Response($response_body);
    $response->headers->add(['Content-Type' => 'Application/JSON']);
    $response = (new DiactorosFactory())->createResponse($response);

    return $response;
  }

  public function decode(RequestInterface $request)
  {
    $token_string = $request->getHeader('Authorization');
    $token_string = $token_string[0];
    $token = Token::decode($token_string, getenv('TOKEN_SECRET'))->toArray();

    $response = new ResponseBody();
    $response->setData($token);


    $response = new \Symfony\Component\HttpFoundation\Response($response);
    $response->headers->add(['Content-Type' => 'Application/JSON']);
    $response = (new DiactorosFactory())->createResponse($response);
    return $response;
  }
}
