<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-04-08
 * Time: 17:47
 */

namespace App\Application\Controller;


use App\Application\Core\Controller\AbstractController;
use App\Application\Repository\UserRepository;
use App\Application\Storage\User\UserStorageMongoDB;
use App\Domain\UseCases\CreateUserUseCase;
use App\Infrastructure\Http\ResponseBody;
use MongoDB\Driver\Manager;
use Psr\Http\Message\RequestInterface;
use Symfony\Bridge\PsrHttpMessage\Factory\DiactorosFactory;

class UserController extends AbstractController
{
  public function get(RequestInterface $request, Manager $manager)
  {
    $storage = new UserStorageMongoDB($manager, getenv('MONGO_DB_NAME'));
    $repository = new UserRepository($storage);
    $users = $repository->getByStatus(true);

    foreach ($users as &$user) {
      $user = $user->toArray();
    }

    $response_body = new ResponseBody();
    $response_body->setData($users);

    $response = new \Symfony\Component\HttpFoundation\Response($response_body);
    $response->headers->add(['Content-Type' => 'Application/JSON']);
    $response = (new DiactorosFactory())->createResponse($response);

    return $response;
  }

  public function getUserById(RequestInterface $request, string $id, Manager $manager)
  {
    $storage = new UserStorageMongoDB($manager, getenv('MONGO_DB_NAME'));
    $repository = new UserRepository($storage);

    $user = $repository->getByUniqueId($id);

    $response_body = new ResponseBody();
    $response_body->setData($user->toArray());

    $response = new \Symfony\Component\HttpFoundation\Response($response_body);
    $response->headers->add(['Content-Type' => 'Application/JSON']);
    $response = (new DiactorosFactory())->createResponse($response);

    return $response;
  }

  public function create(RequestInterface $request, Manager $manager)
  {
    $body = json_decode($request->getBody()->getContents(), true);

    $storage = new UserStorageMongoDB($manager, getenv('MONGO_DB_NAME'));
    $repository = new UserRepository($storage);

    $usecase = new CreateUserUseCase($repository);
    $result = $usecase->run($body);

    $response_body = new ResponseBody();
    $response_body->setData($result);

    $response = new \Symfony\Component\HttpFoundation\Response($response_body);
    $response->headers->add(['Content-Type' => 'Application/JSON']);
    $response = (new DiactorosFactory())->createResponse($response);

    return $response;
  }

  public function delete(RequestInterface $request, $id, Manager $manager)
  {

  }

  public function update(RequestInterface $request, $id, Manager $manager)
  {
  }
}
