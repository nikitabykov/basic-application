<?php


namespace App\Application\Storage\Filter;


use App\Application\Core\Storage\FilterInterface;

class MongoFilter implements FilterInterface
{

  private $filter = [];

  public function equal(string $key, $value): FilterInterface
  {
    $this->filter[$key]['$eq'] = $value;
    return $this;
  }

  public function notEqual(string $key, $value): FilterInterface
  {
    $this->filter[$key]['$ne'] = $value;
    return $this;
  }

  public function in(string $key, array $values): FilterInterface
  {
    $this->filter[$key]['$in'] = $values;
    return $this;
  }

  public function notIn(string $key, array $values): FilterInterface
  {
    $this->filter[$key]['$nin'] = $values;
    return $this;
  }

  public function lower(string $key, $value): FilterInterface
  {
    $this->filter[$key]['$lt'] = $value;
    return $this;
  }

  public function greater(string $key, $value): FilterInterface
  {
    $this->filter[$key]['$gt'] = $value;
    return $this;
  }

  public function lowerOrEqual(string $key, $value): FilterInterface
  {
    $this->filter[$key]['$lte'] = $value;
    return $this;
  }

  public function greaterOrEqual(string $key, $value): FilterInterface
  {
    $this->filter[$key]['$gte'] = $value;
    return $this;
  }

  public function getFilter(): array
  {
    return $this->filter;
  }
}