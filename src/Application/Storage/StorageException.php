<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-04-09
 * Time: 10:36
 */

namespace App\Application\Storage;


use App\Application\ApplicationException;

class StorageException extends ApplicationException
{

}