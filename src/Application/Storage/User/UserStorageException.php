<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-04-09
 * Time: 10:36
 */

namespace App\Application\Storage\User;


use App\Application\Storage\StorageException;

class UserStorageException extends StorageException
{

}