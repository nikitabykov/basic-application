<?php
/**
 * Created by PhpStorm.
 * User: nbykov
 * Date: 2019-04-08
 * Time: 18:57
 */

namespace App\Application\Storage\User;


use App\Application\Core\Storage\FilterInterface;
use App\Application\Core\Storage\StorageInterface;
use App\Domain\Entity\User\Events\UserCreatedEvent;
use App\Domain\Event\EventInterface;
use MongoDB\Driver\Manager;
use MongoDB\Driver\Query;

class UserStorageMongoDB implements StorageInterface
{

  private $manager = null;
  private $db_name = null;
  private $namespace = null;

  const COLLECTION_NAME = 'users';

  public function __construct(Manager $manager, string $db_name)
  {
    $this->manager = $manager;
    $this->db_name = $db_name;
    $this->namespace = sprintf('%s.%s', $db_name, static::COLLECTION_NAME);
  }

  public function handle(EventInterface $event)
  {
    if ($event instanceof UserCreatedEvent) {
      $data = $event->toArray();
      $bulk = new \MongoDB\Driver\BulkWrite();
      $bulk->insert($data);
      $this->manager->executeBulkWrite($this->namespace, $bulk);
    }
  }

  public function getByFilter(FilterInterface $filter, int $offset = 0, int $size = 0): array
  {
    $query = new Query($filter->getFilter(), []);

    $cursor = $this->manager->executeQuery($this->namespace, $query);

    return $cursor->toArray();
  }
}
